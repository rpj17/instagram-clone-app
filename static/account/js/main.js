const profilePanelButtons = document.querySelectorAll('.profile-panel .panel-block'),
      profilePanelContent = document.querySelectorAll('.profile-content .content'),
      NavbarDropdownLinks = document.querySelectorAll('.navbar-item.has-dropdown'),
      ProfileButtonImage = document.querySelectorAll('.profile-button-image'),
      filename = document.querySelector('span.file-name'),
      upload_fields = document.querySelectorAll("div.file.has-name"),
      $avatar = document.querySelector('#profile-avatar-img'),
      textareaBiography = document.querySelector('.biography-textarea'),
      $containerPreviewImg = document.querySelector('.preview_upload_image'),
      $likeButtons = document.querySelectorAll('.like-button'),
      $commentButtons = document.querySelectorAll('.btn-comment-jump');

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (cook of cookies) {
            let cookie = cook.trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const csrftoken = getCookie('csrftoken');

profilePanelButtons.forEach(el => {
    el.addEventListener('click', e => {
        profilePanelButtons.forEach(el => {
            el.classList.remove('is-active');
        });

        profilePanelContent.forEach(el => {
            el.classList.add('is-hidden');
        });

        switch (el.dataset.button_number) {
            case "1":
                // el.classList.add('is-active');
                // document.querySelector('.content-1').classList.remove('is-hidden');
                showEditProfileSettings(el, 1);
                break;
            case "2":
                // el.classList.add('is-active');
                // document.querySelector('.content-2').classList.remove('is-hidden');
                showEditProfileSettings(el, 2);
                break;
            case "3":
                // el.classList.add('is-active');
                // document.querySelector('.content-3').classList.remove('is-hidden');
                showEditProfileSettings(el, 3);
                break;
            case "4":
                // el.classList.add('is-active');
                // document.querySelector('.content-4').classList.remove('is-hidden');
                showEditProfileSettings(el, 4);
                break;
            case "5":
                // el.classList.add('is-active');
                // document.querySelector('.content-5').classList.remove('is-hidden');
                showEditProfileSettings(el, 5);
                break;
        }
    });
});

function showEditProfileSettings(el, number) {
    el.classList.add('is-active');
    document.querySelector('.content-' + number).classList.remove('is-hidden');
}

NavbarDropdownLinks.forEach(el => {
    el.addEventListener('click', e => {
        el.classList.toggle('is-active');
    });
});

ProfileButtonImage.forEach(el => {
    el.addEventListener('click', e => {
        $imageModal = el.nextElementSibling;
        $ModalBackgrounds = document.querySelectorAll('.modal .modal-background');
        $CloseModalButtons = document.querySelectorAll('.modal .modal-close');
        $imageModal.classList.add('is-active');

        $ModalBackgrounds.forEach(el => {
            el.addEventListener('click', () => {
                $imageModal.classList.remove('is-active');
            });
        });

        $CloseModalButtons.forEach(el => {
            el.addEventListener('click', () => {
                $imageModal.classList.remove('is-active');
            });
        });
    });
});


const options = {
    type: "date",
    position: "auto",
    showHeader: false,
    // dateFormat: "dd-mm-yy", 
};
const calendars = bulmaCalendar.attach('[type="date"]', options);

calendars.forEach(calendar => {
    calendar.on('date:selected', date => {
        // console.log(date);
    });

    // console.log(calendar.dateFormat)
});

// upload_fields.forEach(field => {
//     let input = field;
//     // let input = field;

//     input.addEventListener('change', () => {
//         filename.classList.remove('is-hidden');
//         // $avatar.setAttribute('src', input.value);
//         // console.log(input.value)
//     });
// });

const fileInputs = document.querySelectorAll('.file-input');

fileInputs.forEach(input => {
    input.addEventListener('change', () => {
        let $filename = input.nextElementSibling.nextElementSibling;
        let $img = $containerPreviewImg.firstElementChild;
        let $file = input.files[0];

        $filename.classList.remove('is-hidden');
        $filename.textContent = $file.name;
        
        $img.src = window.URL.createObjectURL($file);
        $img.onload = () => window.URL.revokeObjectURL($file);
    });
});

if (textareaBiography) {
    textareaBiography.textContent = textareaBiography.textContent.trim();
}

$likeButtons.forEach(btn => {
    btn.addEventListener('click', e => {
        e.preventDefault();
        fetch(`${window.location.origin}/publications/like/`, {
            method: 'POST',
            headers: {
                'X-CSRFToken': csrftoken,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
            },
            body: JSON.stringify({
                id: btn.dataset.id,
                action: btn.dataset.action,
            }),
            credentials: 'include'
            // mode: 'cors', // no-cors, cors, *same-origin,
        })
        .then(res => {
            return res.json();
        })
        .then(data => {
            let actionBtns = document.querySelectorAll('a.like-button');

            actionBtns.forEach(button => {
                let previousAction = button.dataset.action;
                let previousLikes = button.nextElementSibling;
                let iconBg = button.firstElementChild;
                let likesNumber = parseInt(previousLikes.textContent);

                // const usersLikes = getUsersLikes(button);

                button.dataset.action = previousAction === 'like' ? 'unlike' : 'like';
                previousLikes.textContent = previousAction === 'like' ? likesNumber + 1 : likesNumber - 1;
                previousAction === 'like' ? iconBg.classList.add('has-text-primary') : iconBg.classList.remove('has-text-primary');
            });
        })
        .catch(error => console.log(error));
    });
});

// function getUsersLikes(el) {
//     let usersLikes = null;

//     fetch(`${window.location.origin}/publications/users_likes/`, {
//         method: 'POST',
//         headers: {
//             'X-CSRFToken': csrftoken,
//             'Content-Type': 'application/json',
//             'X-Requested-With': 'XMLHttpRequest',
//         },
//         body: JSON.stringify({
//             id: el.dataset.id
//         }),
//         credentials: 'include'
//     })
//     .then(res => res.json())
//     .then(data => {
//         console.log(data);
//     })
//     .catch(err => console.log(err));
// }

$commentButtons.forEach(btn => {
    btn.addEventListener('click', e => {
        e.preventDefault();
        let textareas = document.querySelectorAll('#id_comment');
        textareas.forEach(textarea => {
            textarea.focus();
        });
    });
});

let page = 1,
    emptyPage = false
    blockRequest = false;

window.addEventListener('scroll', () => {
    const margin = document.documentElement.scrollTop - document.documentElement.scrollTop - 200;
    if (window.scrollY > margin && emptyPage === false && blockRequest === false) {
        blockRequest = true;
        page += 1;
        fetch(`${window.location.origin}/publications/?page=${page}`, {
            method: 'GET'
        })
        // .then(res => res.json())
        .then(data => {
            if (data === '') emptyPage = true;
            blockRequest = false;
            console.log(data)
            // document.querySelector('.publications_list').append(data.body);
            $('.publications_list').append(data);
            // const reader = data.body.getReader();
            // const stream = new ReadableStream({
            //     start(controller) {
            //         function push() {
            //             reader.read().then(({done, value}) => {
            //                 if (done) {
            //                     controller.close()
            //                     return
            //                 }
            //                 controller.enqueue(value)
            //                 push()
            //             })
            //         }

            //         push()
            //     }
            // });

            // return new Response(stream, {headers: {'Content-Type': 'text/html'}})
        })
    }
});
