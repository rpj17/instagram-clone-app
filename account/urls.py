from django.urls import path, reverse_lazy
from . import views
from django.contrib.auth import views as auth_views

app_name = 'account'

urlpatterns = [
	path('login/', auth_views.LoginView.as_view(template_name="account/login.html"), name="login"),
	path('register/', views.register, name="register"),
	path('dashboard/', views.dashboard, name="dashboard"),
	path('profile/', views.profile, name="profile"),
	path('profile/edit', views.edit, name="edit"),
	path('logout/', views.logout_view, name="logout"),
	path('change-password/', views.change_password, name="change_password"),
	path('users/follow/', views.user_follow, name="user_follow"),
	path('users/', views.user_list, name="user_list"),
	path('user/<username>/', views.user_detail, name="user_detail"),
	path('', views.redirect_to_profile, name="redirect_to_profile"),
	# path('publications_list/', views.publication_list, name="publications_list"),
	# path('change-password/', auth_views.PasswordChangeView.as_view(template_name="account/edit_account.html", success_url=reverse_lazy('account:edit')), name="change_password"), # reverse_lazy('password_change_done')
	# path('change-password/done', auth_views.PasswordChangeDoneView.as_view(template_name="account/profile.html"), name="change_password_done"),
]