from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings

def user_directory_path(instance, filename):
	return 'users/{0}/avatar/{1}'.format(instance.user.username, filename)

class Contact(models.Model):
	user_from = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='rel_from_set', on_delete='CASCADE')
	user_to = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='rel_to_set', on_delete='CASCADE')
	created = models.DateTimeField(auto_now_add=True, db_index=True)

	class Meta:
		ordering = ['-created']

	def __str__(self):
		return '{} follows {}'.format(self.user_from, self.user_to)

class CustomUser(AbstractUser):
	first_name = models.CharField('first name', max_length=30, blank=False)
	last_name = models.CharField('last name', max_length=150, blank=False)
	email = models.EmailField('email address', blank=False)
	following = models.ManyToManyField('self', through=Contact, related_name='followers', symmetrical=False)

class Profile(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete='CASCADE')
	date_of_birth = models.DateField(blank=True, null=True)
	biography = models.TextField(blank=True, null=True)
	avatar = models.ImageField(upload_to=user_directory_path, blank=True) #'users/%Y/%m/%d'
	phone = models.CharField('phone', max_length=30, blank=True)

	def __str__(self):
		return 'Profile for user: {}'.format(self.user.username)


	
