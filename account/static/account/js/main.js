const profilePanelButtons = document.querySelectorAll('.profile-panel .panel-block'),
      profilePanelContent = document.querySelectorAll('.profile-content .content'),
      NavbarDropdownLinks = document.querySelectorAll('.navbar-item.has-dropdown'),
      $btnsProfileModalPub = document.querySelectorAll('.profile-button-image'),
      $modalBgs = document.querySelectorAll('.modal .modal-background'),
      $closeBtns = document.querySelectorAll('.modal .modal-close'),
      filename = document.querySelector('span.file-name'),
      upload_fields = document.querySelectorAll("div.file.has-name"),
      $avatar = document.querySelector('#profile-avatar-img'),
      textareaBiography = document.querySelector('.biography-textarea'),
      $containerPreviewImg = document.querySelector('.preview_upload_image'),
      $likeBtns = document.querySelectorAll('.like-button'),
      $followBtns = document.querySelectorAll('.button-follow'),
      $commentBtns = document.querySelectorAll('.btn-comment-jump'),
      $dashboardCommentBtns = document.querySelectorAll('.btn-dashboard-comment-jump'),
      $textareas = document.querySelectorAll('#id_comment'),
      $dashboardTextareas = document.querySelectorAll('.dashboard_comment_textarea');

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (cook of cookies) {
            let cookie = cook.trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const csrftoken = getCookie('csrftoken');

profilePanelButtons.forEach(el => {
    el.addEventListener('click', e => {
        profilePanelButtons.forEach(el => {
            el.classList.remove('is-active');
        });

        profilePanelContent.forEach(el => {
            el.classList.add('is-hidden');
        });

        switch (el.dataset.button_number) {
            case "1":
                showEditProfileSettings(el, 1);
                break;
            case "2":
                showEditProfileSettings(el, 2);
                break;
            case "3":
                showEditProfileSettings(el, 3);
                break;
            case "4":
                showEditProfileSettings(el, 4);
                break;
            case "5":
                showEditProfileSettings(el, 5);
                break;
        }
    });
});

function showEditProfileSettings(el, number) {
    el.classList.add('is-active');
    document.querySelector('.content-' + number).classList.remove('is-hidden');
}

NavbarDropdownLinks.forEach(el => {
    el.addEventListener('click', e => {
        el.classList.toggle('is-active');
    });
});

const options = {
    type: "date",
    position: "auto",
    showHeader: false,
    // dateFormat: "dd-mm-yy", 
};
const calendars = bulmaCalendar.attach('[type="date"]', options);

calendars.forEach(calendar => {
    calendar.on('date:selected', date => {
        // console.log(date);
    });

    // console.log(calendar.dateFormat)
});

const fileInputs = document.querySelectorAll('.file-input');

fileInputs.forEach(input => {
    input.addEventListener('change', () => {
        let $filename = input.nextElementSibling.nextElementSibling;
        let $img = $containerPreviewImg.firstElementChild;
        let $file = input.files[0];

        $filename.classList.remove('is-hidden');
        $filename.textContent = $file.name;
        
        $img.src = window.URL.createObjectURL($file);
        $img.onload = () => window.URL.revokeObjectURL($file);
    });
});

if (textareaBiography) {
    textareaBiography.textContent = textareaBiography.textContent.trim();
}

handleProfilePubModal($btnsProfileModalPub, $modalBgs, $closeBtns);
handleLikeBtns($likeBtns);
handleCommentBtns($commentBtns, $textareas);
handleDashboardCommentBtns($dashboardCommentBtns, $dashboardTextareas);
handleFollowBtns($followBtns);

function handleProfilePubModal(btns, modalBgs, closeBtns) {
    btns.forEach(btn => {
        btn.addEventListener('click', e => {
            e.preventDefault();
            const imageModal = btn.nextElementSibling;

            imageModal.classList.add('is-active');

            modalBgs.forEach(el => {
                el.addEventListener('click', () => {
                    imageModal.classList.remove('is-active');
                });
            });

            closeBtns.forEach(el => {
                el.addEventListener('click', () => {
                    imageModal.classList.remove('is-active');
                });
            });
        })
    });
}

function handleLikeBtns(btns) {
    btns.forEach(btn => {
        btn.addEventListener('click', e => {
            e.preventDefault();
            fetch(`${window.location.origin}/publications/like/`, {
                method: 'POST',
                headers: {
                    'X-CSRFToken': csrftoken,
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                },
                body: JSON.stringify({
                    id: btn.dataset.id,
                    action: btn.dataset.action,
                }),
                credentials: 'include'
            })
            .then(res => res.json())
            .then(data => {
                const previousAction = btn.dataset.action,
                      previousLikes = btn.nextElementSibling,
                      iconBg = btn.firstElementChild,
                      likesNumber = parseInt(previousLikes.textContent);

                btn.dataset.action = previousAction === 'like' ? 'unlike' : 'like';
                previousLikes.textContent = previousAction === 'like' ? likesNumber + 1 : likesNumber - 1;
                previousAction === 'like' ? iconBg.classList.add('has-text-primary') : iconBg.classList.remove('has-text-primary');
            })
            .catch(error => console.log(error));
        });
    });
}

function handleCommentBtns(btns, textareas) {
    btns.forEach(btn => {
        btn.addEventListener('click', e => {
            e.preventDefault();
            textareas.forEach(textarea => {
                textarea.focus();
            });
        });
    });   
}

function handleDashboardCommentBtns(btns, textareas) {
    btns.forEach(btn => {
        btn.addEventListener('click', e => {
            e.preventDefault();
            textareas.forEach(textarea => {
                let buttonNumber = btn.dataset.dashboard_button_comment_number,
                    textareaNumber = textarea.dataset.dashboard_textarea_number;

                if (buttonNumber === textareaNumber) {
                    textarea.focus();
                }
            });
        });
    });   
}

function handleFollowBtns(btns) {
    btns.forEach(btn => {
        btn.addEventListener('click', makeFollowRequest);
    });
}

async function makeFollowRequest(e) {
    const btn = document.querySelector('.button-follow.follow-users');
    e.preventDefault();
    const res = await fetch(`${window.location.origin}/account/users/follow/`, {
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRFToken': csrftoken,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            id: btn.dataset.id,
            action: btn.dataset.action,
        })
    });
    const data = await res.json();

    if (data.status === 'ok') {
        const previousAction = btn.dataset.action;
        const previousFollowers = document.querySelector('.total-followers-number');

        btn.dataset.action = previousAction === 'follow' ? 'unfollow' : 'follow';
        btn.textContent = previousAction === 'follow' ? 'unfollow' : 'follow';
        
        if (previousAction === 'follow') {
            previousFollowers.textContent = parseInt(document.querySelector('.total-followers-number').textContent) + 1;
        } else {
            previousFollowers.textContent = parseInt(document.querySelector('.total-followers-number').textContent) - 1;
        }
    }
}

// let page = 1,
//     emptyPage = false
//     blockRequest = false;
// if (window.location.pathname == '/account/profile/') {
//     window.addEventListener('scroll', () => {
//         if ((window.scrollY + document.documentElement.clientHeight) === document.documentElement.scrollHeight && emptyPage === false && blockRequest === false) {
//             blockRequest = true;
//             page += 1;
//             fetch(`${window.location.origin}/account/profile/?page=${page}`, {
//                 method: 'GET',
//                 headers: {
//                     Accept: 'text/html',
//                     'X-Requested-With': 'XMLHttpRequest',
//                 }
//             })
//             .then(res => res.text())
//             .then(text => {
//                 if (text === '') emptyPage = true;
//                 blockRequest = false;

//                 const parser = new DOMParser(),
//                       doc = parser.parseFromString(text, 'text/html'),
//                       publications = doc.querySelectorAll('.publication'),
//                       btns = doc.querySelectorAll('.publication .profile-button-image'),
//                       modalBgs = doc.querySelectorAll('.modal .modal-background'),
//                       closeBtns = doc.querySelectorAll('.modal .modal-close'),
//                       likeBtns = doc.querySelectorAll('.like-button'),
//                       commentBtns = doc.querySelectorAll('.btn-comment-jump'),
//                       textareas = doc.querySelectorAll('#id_comment');

//                 handleProfilePubModal(btns, modalBgs, closeBtns);
//                 handleLikeBtns(likeBtns);
//                 handleCommentBtns(commentBtns, textareas);

//                 publications.forEach(pub => {
//                     document.querySelector('.publications-grid').append(pub);
//                 });
//             })
//         }
//     });
// }


// upload_fields.forEach(field => {
//     let input = field;
//     // let input = field;

//     input.addEventListener('change', () => {
//         filename.classList.remove('is-hidden');
//         // $avatar.setAttribute('src', input.value);
//         // console.log(input.value)
//     });
// });

// if (window.location.pathname == '/publications/') {
//     $(window).scroll(function() {
//         let = margin = $(document).height() - $(window).height() - 200;

//         if ($(window).scrollTop() > margin && emptyPage == false &&
//             blockRequest == false) {
//             blockRequest = true;
//             page += 1;
//             $.get('?page=' + page, function(data) {
//                 if(data == '') {
//                     emptyPage = true;
//                 } else {
//                     blockRequest = false;
//                     $('.publications_list').append(data);
//                     console.log(data)
//                 }  
//             });
//         }
//     });  
// }

