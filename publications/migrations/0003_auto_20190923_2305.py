# Generated by Django 2.2.2 on 2019-09-24 03:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publications', '0002_auto_20190923_2158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publication',
            name='url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
