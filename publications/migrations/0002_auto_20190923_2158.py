# Generated by Django 2.2.2 on 2019-09-24 01:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publication',
            name='url',
            field=models.URLField(blank=True),
        ),
    ]
