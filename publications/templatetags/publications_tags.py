from django import template
from publications.models import Publication

register = template.Library()

@register.simple_tag(name="pubs_minus_one")
def number_of_publications_minus_one(pub_id):
	pub = Publication.objects.get(id=pub_id)
	number_of_users_likes = pub.users_like.count()
	number_minus_one = int(number_of_users_likes) - 1
	if (number_minus_one < 0): 
		return 0
	return number_minus_one