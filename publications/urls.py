from django.urls import path
from . import views

app_name = 'publications'

urlpatterns = [
	path('create/', views.create_publication, name="create_publication"),
	path('like/', views.publication_like, name="like"),
	path('', views.publication_list, name="list"),
	path('comment_publication/', views.publication_comments_list, name="comment_publication"),
	path('delete_comment/', views.delete_comment, name="delete_comment"),
	# path('users_likes/', views.get_users_likes, name="get_users_likes"),
]